# Download and use the wallet clients #
## Installing the Genexcoin wallet for Windows OS ##

**1.** Trust at any time the files contained into this repository because are free of virus.

**2.** Ignore any antivirus advise.

**3.** Download the .zip file that contains the wallet from here [from here](https://sourceforge.net/projects/genexcoin-windows-client).

**4.** Extract the files.

**5.** Change the file extension adding ".exe" at the ends of any file to make it executable. Execute the .exe file named "genexcoin-qt.exe".

**6.** Add any rule asked for your firewall, if is it.

**7.** That's all. You're ready to do all transactions you need on the Genexcoin net.

### Starting to mine Genexcoins with CPU ###

This feature will be available soon.

### Connecting to a specific previously created node ###

You have the option to connect yourself to a specific Node trough the Node IP address, for that do the next:

**1.** Open your datafiles folder writing the command _%appdata%_ into the cmd program.

**2.** Create a file named _genexcoin.conf_ and then copy and paste within it the next code:

```
addnode=IP_ADDRESS
```

**3.** Replace <IP_ADDRESS> with the hostname or IP address suplied and save the file, they (IP addresses) will be updated weekly.

 That's all, you should be connected to a Node.

 ## Download and use wallet clients for Linux, McOS and raspberry ##
 These features will be available soon.

 Wait for news.
 

